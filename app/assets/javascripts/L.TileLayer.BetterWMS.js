L.TileLayer.BetterWMS = L.TileLayer.WMS.extend({

  onAdd: function (map) {
    L.TileLayer.WMS.prototype.onAdd.call(this, map);
    this.getLegend(map);
  },

  onRemove: function (map) {
    L.TileLayer.WMS.prototype.onRemove.call(this, map);
    $('div[id="'+this.options.layers+'"]').remove();
  },

  getLegend: function(map){
    var url=document.createElement("img");
    url.src = this._url+"?layer="+this.options.layers+"&Mode=legend";
    var src = $("#legend-panel");
    src.append('<div id="'+this.options.layers+'" class="padding-left-right"><p class="center-align w-margin"><strong>'+this.options.layers+'</strong></p><img src="'+this._url+"?layer="+this.options.layers+"&Mode=legend"+'"><div class="divider"></div></div>');
  }
});

L.tileLayer.betterWms = function (url, options) {
  return new L.TileLayer.BetterWMS(url, options);
};
