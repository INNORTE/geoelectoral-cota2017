var map,url,panel,olayers,blayers,i,k,elem;
function layerControl(m,u){
  setValues('#layers-panel',m,u);
  addTitleToGroupLayers('Capas Base',panel);
  addLayersToPanel(blayers,panel);
  addTitleToGroupLayers('Capas Temáticas',panel);
  addLayersToPanel(olayers,panel);
}

function setValues(id,m,u){
  panel=$(id);
  map=m;
  url=u;
  blayers=getBaseLayers(m);
  olayers=getOverLayers(m,u);
}

function addLayersToPanel(obj,panel){
  var addEventToObject;
  for(key in obj){
    panel.append('<li id="'+key+'" onClick="toggleLayer(this)"><a>'+key+'</a></li>');
  }
  return ;
}

function addTitleToGroupLayers(title,panel){
  panel.append('<li class="center-align"><h6><strong>'+title+'</strong></h6></li>');
  return ;
}

function toggleLayer(obj){
  if(map.hasLayer(olayers[obj.id])){
    map.removeLayer(olayers[obj.id]);
    obj.className = '';
  }else{
    olayers[obj.id].addTo(map);
    obj.className += 'active';
  }
}

function getFeatureInfo(host,coords,callback){
  // Get Layers Active on map
  layOnMap = getLayersOnMap();
  // Make Ajax request
  if(layOnMap.length > 0){
    $.ajax({
      url:'/getFeatureInfo.json',
      method : "GET",
      data: {'coordenadas':coords,'layers':layOnMap},
      dataType : "json",
      success : callback,
      error: function(e,xhr){
        console.error('Error mandado a través del servidor: '+e);
      }
    });
  }else {
    console.log('No hay capas en el objeto map')
  }
}

function showPopUpData(host,coords){
  //var prom = makeAjax(activeLayers);
  //prom.then
}

function getLayersOnMap(){
  var layOnMap=[];
  for(key in olayers){
    if(map.hasLayer(olayers[key])){layOnMap.push(key)}
  }
  return layOnMap;
}

function formatingResponse(resp){
  var content = '<div id="featureInfo" class="row"><div class="col s12"><ul class="tabs">',hash;
  // Por cada capa se crea una pestaña y también se crea el div del contenido de la pestaña
  for(key in resp){
    // Lista de pestañas
    content += '<li class="tab"><a href="#tab'+key+'">'+Object.keys(resp[key])+'</a></li>';
  }
  content += '</ul></div>';
  for(key in resp){
    // Se crea el contenido de las pestañas y cabecera de la tabla
    content += '<div id="tab'+key+'" class="col s12">';
    hasValue = resp[key][Object.keys(resp[key])];
    if(hasValue !== null){
      content +='<table class="responsive-table"><thead><tr>';
      // Obtener el objeto
      hash = hasValue["0"];
      // Initialize containers
      columns='';
      values = '';
      // Se crea las columnas de campos y valores
      for(val in hash){
        if(val != 'geom' && val != 'id' && val != 'gid' && val != 'shape_length' && val != 'shape_area'){
        columns += '<th>'+val+'</th>';
        values += '<td>'+hash[val]+'</td>';
        }
      }
      content += columns+'</tr></thead><tbody>'+values+'</tbody></table>';
    }else{
      content += '<div class="center-align"><h5>No se encontraron datos asociados</h5></div>';
    }
    content += '</div>'
  }
  return content;
}
