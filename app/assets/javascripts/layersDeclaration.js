function getBaseLayers(map){
  // Base Layers Declaration
  return {
    'OpenStreetMap BaseLayer': L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(map)
  };
}

function getOverLayers(map,url){
  // Over layers declaration
  var compe11=L.layerGroup([L.tileLayer.betterWms(url,{layers:'Lealtad Electoral',transparent:true,format:'image/png'}),
              L.tileLayer.betterWms(url,{layers:'Competitividad Electoral - Gobernador 11',transparent:true,format:'image/png'})]),
      compe99=L.layerGroup([L.tileLayer.betterWms(url,{layers:'Lealtad Electoral',transparent:true,format:'image/png'}),
                  L.tileLayer.betterWms(url,{layers:'Competitividad Electoral - Gobernador 99',transparent:true,format:'image/png'})]);

  return {
    //  Thematic layers
    'Secciones': L.tileLayer.betterWms(url,{layers:'Secciones', transparent:true,format:'image/png'}),
    'Distrito Estatal': L.tileLayer.betterWms(url,{layers:'Distritos Estatales', transparent:true,format:'image/png'}),
    'Distrito Federal': L.tileLayer.betterWms(url,{layers:'Distritos Federales',transparent:true,format:'image/png'}),
    'Límite Municipal': L.tileLayer.betterWms(url,{layers:'Marco Geoestadístico Municipal',transparent:true,format:'image/png'}),
    'Límite Estatal': L.tileLayer.betterWms(url,{layers:'Marco Geoestadístico Estatal',transparent:true,format:'image/png'}),
    'Colonias': L.tileLayer.betterWms(url,{layers:'Colonias',transparent:true,format:'image/png'}),
    'Vías Ferreas': L.tileLayer.betterWms(url,{layers:'Vias Ferreas',transparent:true,format:'image/png'}),
    'Uso de Suelo y Vegetación': L.tileLayer.betterWms(url,{layers:'Uso de Suelo y Vegetación',transparent:true,format:'image/png'}),
    'Infraestructura': L.tileLayer.betterWms(url,{layers:'Infraestructura',transparent:true,format:'image/png'}),
    'Carreteras': L.tileLayer.betterWms(url,{layers:'Carreteras',transparent:true,format:'image/png'}),
    'Caminos': L.tileLayer.betterWms(url,{layers:'Caminos',transparent:true,format:'image/png'}),
    'Núcleo Agrario': L.tileLayer.betterWms(url,{layers:'Núcleo Agrario',transparent:true,format:'image/png'}),
    'Estrategia Electoral - 2011': compe11,
    'Estrategia Electoral - 1999': compe99,
    //  Analized Layers
    //  ABC layers
    'ABC - Sección' : L.tileLayer.betterWms(url,{layers:'ABC - Seccion',transparent:true,format:'image/png'}),
    //'ABC - Municipio' : L.tileLayer.betterWms(url,{layers:'ABC - Municipio',transparent:true,format:'image/png'}),
    //'ABC - Distrito Estatal' : L.tileLayer.betterWms(url,{layers:'ABC - Distrito Estatal',transparent:true,format:'image/png'}),
    //'ABC - Distrito Federal' : L.tileLayer.betterWms(url,{layers:'ABC - Distrito Federal',transparent:true,format:'image/png'}),
    //'Meta por Sección Electoral': L.tileLayer.betterWms(url,{layers:'Meta por Sección Electoral',transparent:true,format:'image/png'}),
    'Lealtad Electoral': L.tileLayer.betterWms(url,{layers:'Lealtad Electoral',transparent:true,format:'image/png'}),
    'Volatilidad Electoral': L.tileLayer.betterWms(url,{layers:'Volatilidad Electoral',transparent:true,format:'image/png'}),

    //  Competitividad Electoral
    'Competitividad Electoral - Gobernador 99':L.tileLayer.betterWms(url,{layers:'Competitividad Electoral - Gobernador 1999',transparent:true,format:'image/png'}),
    'Competitividad Electoral - Gobernador 05':L.tileLayer.betterWms(url,{layers:'Competitividad Electoral - Gobernador 2005',transparent:true,format:'image/png'}),
    'Competitividad Electoral - Gobernador 11':L.tileLayer.betterWms(url,{layers:'Competitividad Electoral - Gobernador 2011',transparent:true,format:'image/png'}),

    //  Resultados Cota
    'Partido Ganador - Presidencia Mpal 05': L.tileLayer.betterWms(url,{layers:'Partido Ganador - Presidencia Municipal 2005', transparent:true,format:'image/png'}),
    'Competitividad Electoral - Presidencia Mpal 05':L.tileLayer.betterWms(url,{layers:'Competitividad Electoral - Presidencia Municipal 2005', transparent:true,format:'image/png'}),
    'Partido Ganador - Diputado Local 02': L.tileLayer.betterWms(url,{layers:'Partido Ganador - Diputado Local 2002', transparent:true, format:'image/png'}),
    'Competitividad Electoral - Diputado Local 02': L.tileLayer.betterWms(url,{layers:'Competitividad Electoral - Diputado Local 2002', transparent:true, format:'image/png'})
  };
}
