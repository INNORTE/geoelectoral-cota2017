document.addEventListener('DOMContentLoaded', function(){
  var map = L.map('map',{zoomControl:false}).setView([21.8,-105],8),host = window.location.hostname,//host='ec2-34-223-231-254.us-west-2.compute.amazonaws.com',//
    url='http://'+host+'/geomapfiles',baseLayers=getBaseLayers(map), overlayers = getOverLayers(map,url), coords=null;
  $('#btn-layers').sideNav();
  layerControl(map,url);
  map.on('click', function(e){
    coords={'lat':e.latlng.lat,'lng':e.latlng.lng};
    getFeatureInfo(host,coords,function(response){
      formResp = formatingResponse(response);
      L.popup({maxWidth: 800})
        .setLatLng(e.latlng)
        .setContent(formResp)
        .openOn(map);
      $('ul.tabs').trigger('$destroy');
      $('ul.tabs').tabs();
    });
  });
}, false);
