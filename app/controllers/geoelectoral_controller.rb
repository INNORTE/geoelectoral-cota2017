class GeoelectoralController < ApplicationController
  before_action :authenticate_user!
  # Get parameters
  before_action :getParams, only: [:getFeatureInfo]

  def index
  end

  def getFeatureInfo
    results = []
    @layers.each do |l|
      t = Translate.where("layer = ?",l)
      results << {l => Translate.new.executeSQL("geo.#{t[0].table}",@coords)}
    end
    # Response to client with data
    respond_to do |format|
      format.html {render html: '<strong>Nada que ver por aquí XD</strong>'.html_safe}
      format.json {render json: results}
      format.xml {render xml: results}
    end
  end

  protected
  def getParams
    # implement a for with layers requested
    @layers = params[:layers]
    @coords = params[:coordenadas]
  end
end
