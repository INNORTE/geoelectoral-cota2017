class Colonias < ApplicationRecord
  self.table_name = 'geo.colonias'
  self.primary_key = 'id'
end
