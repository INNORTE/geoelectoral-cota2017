class Translate < ApplicationRecord

  def executeSQL(table,coords)
    results = ActiveRecord::Base.connection.exec_query("Select * from #{table} where st_intersects(geom,st_transform(st_setsrid(st_point(#{coords['lng']},#{coords['lat']}),4326),3857)) = true")
    return results.to_hash if results.present?
  end
end
