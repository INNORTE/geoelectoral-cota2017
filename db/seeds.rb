# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# Layers traductor
Translate.destroy_all()
Translate.create(layer:'Distrito Federal',table:'distritos_fed_3857')
Translate.create(layer:'Distrito Estatal',table:'distritos_nay')
Translate.create(layer:'Colonias',table:'colonias')
Translate.create(layer:'Competitividad Electoral - Diputado Local 02',table:'resultados_diputadolocal2002')
Translate.create(layer:'Competitividad Electoral - Presidencia Mpal 05',table:'presidente')
Translate.create(layer:'Competitividad Electoral - Gobernador 99',table:'comp_gob_99')
Translate.create(layer:'Competitividad Electoral - Gobernador 05',table:'comp_gob_2005')
Translate.create(layer:'Competitividad Electoral - Gobernador 11',table:'comp_gob_2011')
Translate.create(layer:'Partido Ganador - Diputado Local 02',table:'resultados_diputadolocal2002')
Translate.create(layer:'Partido Ganador - Presidencia Mpal 05',table:'presidente')
Translate.create(layer:'Meta por Sección Electoral',table:'metas2xseccion2017b')
Translate.create(layer:'Lealtad Electoral',table:'lealtad_electoral')
# --------------------------------------------------------------
Translate.create(layer:'ABC - Distrito Estatal',table:'abc_distest')
Translate.create(layer:'ABC - Distrito Federal',table:'abc_distfed')
Translate.create(layer:'ABC - Sección',table:'abc_sec')
Translate.create(layer:'ABC - Municipio',table:'abc_mpio')
Translate.create(layer:'Secciones',table:'secciones')
Translate.create(layer:'Vías Ferreas',table:'vias_ferreas')
Translate.create(layer:'Caminos',table:'vias_caminos')
Translate.create(layer:'Carreteras',table:'vias_carreteras')
Translate.create(layer:'Infraestructura de Transporte',table:'infra_trans')
Translate.create(layer:'Uso de Suelo y Vegetación',table:'usv')
Translate.create(layer:'Núcleo Agrario',table:'nucleo_agrario')
Translate.create(layer:'Límite Estatal',table:'mge_18')
Translate.create(layer:'Límite Municipal',table:'mgm_18')
Translate.create(layer:'Volatilidad Electoral',table:'volatil')
#Translate.create(layer:'Uso de Suelo y Vegetación',table:'usv')
# For future layers
#Translate.create(layer:'',table:'')
