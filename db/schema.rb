# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170409024036) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "postgis"

# Could not dump table "colonias" because of following StandardError
#   Unknown type 'geometry(MultiPolygonZ,3857)' for column 'geom'

# Could not dump table "comp_gob_2005" because of following StandardError
#   Unknown type 'geometry(MultiPolygon,3857)' for column 'geom'

# Could not dump table "comp_gob_2011" because of following StandardError
#   Unknown type 'geometry(MultiPolygon,3857)' for column 'geom'

# Could not dump table "comp_gob_99" because of following StandardError
#   Unknown type 'geometry(MultiPolygon,3857)' for column 'geom'

# Could not dump table "demarcacion" because of following StandardError
#   Unknown type 'geometry(MultiPolygon,3857)' for column 'geom'

# Could not dump table "distritos_fed_3857" because of following StandardError
#   Unknown type 'geometry(MultiPolygon,3857)' for column 'geom'

# Could not dump table "distritos_nay" because of following StandardError
#   Unknown type 'geometry(MultiPolygon,3857)' for column 'geom'

# Could not dump table "lealtad_electoral" because of following StandardError
#   Unknown type 'geometry(MultiPolygon,3857)' for column 'geom'

# Could not dump table "metas2xdist_estfed" because of following StandardError
#   Unknown type 'geometry(MultiPolygon,3857)' for column 'geom'

# Could not dump table "metas2xdist_fec_proc" because of following StandardError
#   Unknown type 'geometry(MultiPolygon,3857)' for column 'geom'

# Could not dump table "metas2xdistr" because of following StandardError
#   Unknown type 'geometry(MultiPolygon,3857)' for column 'geom'

# Could not dump table "metas2xdistritoest" because of following StandardError
#   Unknown type 'geometry(MultiPolygon,3857)' for column 'geom'

# Could not dump table "metas2xseccion" because of following StandardError
#   Unknown type 'geometry(MultiPolygon,3857)' for column 'geom'

# Could not dump table "metas2xseccion2017" because of following StandardError
#   Unknown type 'geometry(MultiPolygon,3857)' for column 'geom'

# Could not dump table "metas2xseccion2017b" because of following StandardError
#   Unknown type 'geometry(MultiPolygon,3857)' for column 'geom'

# Could not dump table "metasxdistr" because of following StandardError
#   Unknown type 'geometry(MultiPolygon,3857)' for column 'geom'

# Could not dump table "metasxseccion" because of following StandardError
#   Unknown type 'geometry(MultiPolygon,3857)' for column 'geom'

# Could not dump table "presidente" because of following StandardError
#   Unknown type 'geometry(MultiPolygon,3857)' for column 'geom'

# Could not dump table "resultados_diputadolocal2002" because of following StandardError
#   Unknown type 'geometry(MultiPolygon,3857)' for column 'geom'

  create_table "spatial_ref_sys", primary_key: "srid", id: :integer, force: :cascade do |t|
    t.string  "auth_name", limit: 256
    t.integer "auth_srid"
    t.string  "srtext",    limit: 2048
    t.string  "proj4text", limit: 2048
  end

  create_table "translates", force: :cascade do |t|
    t.string   "layer"
    t.string   "table"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

# Could not dump table "vota_presumun2005" because of following StandardError
#   Unknown type 'geometry(MultiPolygon,3857)' for column 'geom'

end
