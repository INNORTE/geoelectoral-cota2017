Rails.application.routes.draw do
  devise_for :users,:skip => [:registrations]
  root 'geoelectoral#index'
  get '/getFeatureInfo', to: 'geoelectoral#getFeatureInfo'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
